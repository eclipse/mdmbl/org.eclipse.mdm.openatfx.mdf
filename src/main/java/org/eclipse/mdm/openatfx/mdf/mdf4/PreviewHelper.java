/********************************************************************************
 * Copyright (c) 2015-2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/


package org.eclipse.mdm.openatfx.mdf.mdf4;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.asam.ods.AoException;
import org.asam.ods.ApplicationElement;
import org.asam.ods.ApplicationRelation;
import org.asam.ods.InstanceElement;
import org.asam.ods.InstanceElementIterator;
import org.asam.ods.NameValueUnit;
import org.asam.ods.Relationship;
import org.asam.ods.TS_Union;
import org.asam.ods.TS_Value;
import org.eclipse.mdm.openatfx.mdf.util.ODSHelper;
import org.eclipse.mdm.openatfx.mdf.util.ODSInsertStatement;
import org.eclipse.mdm.openatfx.mdf.util.ODSModelCache;

/**
 * Helper class to convert Sample Reduction blocks to an ASAM ODS
 * preview 'AoMeasurement'.
 *
 * @author Tobias Leemann
 */
class PreviewHelper {

	// the cached lookup instance element
	private long previewMeaiid = Long.MIN_VALUE;
	private ODSModelCache cache;
	private AoSessionWriter writer;

	private Map<String, Long> meqInstances = new HashMap<String, Long>();

	public synchronized void setCache(ODSModelCache cache) {
		this.cache = cache;
	}

	public synchronized void setWriter(AoSessionWriter writer) {
		this.writer = writer;
	}

	/**
	 * Creates a preview Measurment if none exists.
	 * 
	 * @param ieMea
	 *            The InstanceElement of the Main Measurement
	 * @throws AoException
	 *             If an ASAM ODS error occurs.
	 */
	private synchronized void createMeasurementIfNeeded(InstanceElement ieMea) throws AoException {
		// create 'AoMeasurement' instance (if not yet existing)
		if (previewMeaiid == Long.MIN_VALUE) {
			// lookup parent 'AoTest' instance
			InstanceElementIterator iter = ieMea.getRelatedInstancesByRelationship(Relationship.FATHER, "*");
			InstanceElement ieTst = iter.nextOne();
			iter.destroy();
			String meaName = ieMea.getName() + "_previews";
			ODSInsertStatement ins = new ODSInsertStatement(cache, "mea");
			ins.setStringVal("iname", meaName);
			ins.setStringVal("mt", "application/x-asam.aomeasurement.mdf_preview");
			ins.setNameValueUnit(ieMea.getValue("date_created"));
			ins.setNameValueUnit(ieMea.getValue("mea_begin"));
			ins.setNameValueUnit(ieMea.getValue("mea_end"));
			ins.setLongLongVal("tst", ODSHelper.asJLong(ieTst.getId()));
			previewMeaiid = ins.execute();
		}
	}

	/**
	 * Creates Previews for a Channel in all SubMatrix elements created by
	 * createPreviewSubMatrices. This method must be invoked after
	 * createPreviewSubMatrices.
	 * 
	 * @param channelName
	 *            The name of the Channel the preview will be created for.
	 * @param idBlock
	 *            The IDBLOCK.
	 * @param cgBlock
	 *            The CGBLOCK of the Channel.
	 * @param dgBlock
	 *            The DGBLOCK of the Channel.
	 * @param cnBlock
	 *            The CNBLOCK of this Channel.
	 * @param ccBlock
	 *            The CCBLOCK of the Channel.
	 * @param untInstances
	 *            MapContaining all existing Units.
	 * @throws AoException
	 * @throws IOException
	 */
  public synchronized void createPreviewChannels(InstanceElement ieMea, InstanceElement ieLc, String channelName,
      SRBLOCK srBlock, IDBLOCK idBlock, CGBLOCK cgBlock, DGBLOCK dgBlock, CNBLOCK cnBlock, CCBLOCK ccBlock,
      Map<String, Long> untInstances, ApplicationRelation relLcSmPrev) throws AoException, IOException
  {
		if (writer == null) {
			throw new IOException("Preview writer not set.");
		}
		ApplicationElement aeSm = cache.getApplicationElement("sm");
		String[] mimetypeExtensions = { "average", "maximum", "minimum" };
		String[] nameExtensions = { "avg", "max", "min" };
		long lcIid = ODSHelper.asJLong(ieLc.getId());
		
		// for each reduction of the current channel, create a new preview submatrix with the preview channel triple
		while (srBlock != null) {
		  // create SubMatrix for current preview/reduction and link to channel
		  long smIid = createPreviewSubMatrix(ieMea, (int) srBlock.getCycleCount(), getPrevName(srBlock, lcIid));
		  InstanceElement ieSm = aeSm.getInstanceById(ODSHelper.asODSLongLong(smIid));
		  ieLc.createRelation(relLcSmPrev, ieSm);
		  
		  // create the three preview channels for average, maximum and minimum for the current reduction
		  for (int i = 0; i < 3; i++) {
        String extendedName = channelName + "_" + nameExtensions[i];
        // create MeasurementQuantity if needed
        Long iidMeq = meqInstances.get(extendedName);
        if (iidMeq == null) {
          iidMeq = writer.createMeasurementQuantity(cache, cnBlock, ccBlock, null, extendedName, previewMeaiid,
              "application/x-asam.aomeasurementquantity.preview." + mimetypeExtensions[i], untInstances);
          meqInstances.put(extendedName, iidMeq);
        }
        // create AoLocalColumns
        long iidLc = writer.createLocalColumn(cache, dgBlock, cgBlock, cnBlock, ccBlock, null, extendedName,
            -1, smIid, iidMeq, "application/x-asam.aolocalcolumn.preview." + mimetypeExtensions[i]);

        // create AoExternalComponents
        Boolean setGlobalFlag = writer.writeEc(cache, iidLc, idBlock, dgBlock, cgBlock, cnBlock, ccBlock, null,
            srBlock.getLnkRdData(), i + 1);
        
        if (setGlobalFlag != null)
        {
          TS_Union union = new TS_Union();
          union.shortVal(Boolean.TRUE.equals(setGlobalFlag) ? (short) 15 : 0);
          ieLc.setValue(new NameValueUnit("glb", new TS_Value(union, (short) 15), ""));
        }
      }
		  
      srBlock = srBlock.getSrNextBlock();
    }
	}
	
	/**
	 * Create a preview SubMatrix for a channel.
	 * 
	 * @param ieMea
	 * @param rows
	 * @param smName
	 * @return
	 * @throws AoException
	 */
	private synchronized long createPreviewSubMatrix(InstanceElement ieMea, int rows, String smName) throws AoException {
	  createMeasurementIfNeeded(ieMea);
    ODSInsertStatement ins = new ODSInsertStatement(cache, "sm");
    ins.setStringVal("iname", smName);
    ins.setStringVal("mt", "application/x-asam.aosubmatrix.preview");
    ins.setLongVal("rows", rows);
    ins.setLongLongVal("mea", previewMeaiid);
    return ins.execute();
	}

	/**
	 * Create a unique Channel name from an SRBLOCK.
	 * 
	 * @param srBlock
	 *            The SRBLOCK
	 * @return The name.
	 */
	private String getPrevName(SRBLOCK srBlock, long lcIid) {
		String ret = "reduction_group" + lcIid + "_";
		ret += String.valueOf(srBlock.getInterval());
		switch (srBlock.getSyncType()) {
		case 1:
			ret += "s";
			break;
		case 2:
			ret += "rad";
			break;
		case 3:
			ret += "m";
			break;
		case 4:
			ret += "records";
			break;
		default:
			break;
		}
		return ret;
	}
}

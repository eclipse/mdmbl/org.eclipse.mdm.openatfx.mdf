/********************************************************************************
 * Copyright (c) 2015-2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.openatfx.mdf.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Helper class to access configuration parameters outside of AoSessionWriters.
 * Originally created to handle the configuration extension to define
 * replacement strings for TX and MD Blocks, which sometimes contain characters
 * or character sequences invalid for xml or provided with an incorrect
 * character set.
 * 
 * @author Markus Renner
 *
 */
public class ConfigurationHelper
{
  private static final String SEPARATOR_CHARACTER = " ";
  private static final String ENTRY_SEPARATOR_CHARACTER = ":";
  private static ConfigurationHelper instance;
  private Map<String, String> commentReplacementStrings = new HashMap<>();
  
  public static ConfigurationHelper getInstance()
  {
    if (instance == null)
    {
      instance = new ConfigurationHelper();
    }
    return instance;
  }
  
  /**
   * The configured string has following syntax:
   * - replacements are separated by SEPARATOR_CHARACTER
   * - each single replacement contains a ENTRY_SEPARATOR_CHARACTER character, mapping the original to the replacement string
   * 
   * @param configuredString
   */
  public void readReplacementsFromConfiguredString(String configuredString)
  {
    if (configuredString == null || configuredString.isEmpty()) {
      return;
    }
    
    for (String replacementString : configuredString.split(SEPARATOR_CHARACTER)) {
      if (replacementString.contains(ENTRY_SEPARATOR_CHARACTER))
      {
        String[] splits = replacementString.split(ENTRY_SEPARATOR_CHARACTER);
        if (splits.length == 2)
        {
          addCommentReplacementString(splits[0].trim(), splits[1].trim());
        }
      }
    }
  }
  
  public void addCommentReplacementString(String orgString, String replacementString)
  {
    commentReplacementStrings.put(orgString, replacementString);
  }
  
  /**
   * All configured replacements will be traversed and any found occurrences in
   * the given string will be replaced.
   * 
   * @param originalString the original string from a TX or MD Block
   * @return the possibly changed resulting string
   */
  public String replaceStringsInComment(String originalString)
  {
    String editedComment = originalString;
    for (Entry<String, String> entry : commentReplacementStrings.entrySet())
    {
      editedComment = editedComment.replaceAll(entry.getKey(), entry.getValue());
    }
    return editedComment;
  }
}

# Documentation open ATFX MDF4 driver

## Copyright and License ##
Copyright (c) 2015-2020 Contributors to the Eclipse Foundation

 See the NOTICE file(s) distributed with this work for additional
 information regarding copyright ownership.

 This program and the accompanying materials are made available under the
 terms of the Eclipse Public License v. 2.0 which is available at
 http://www.eclipse.org/legal/epl-2.0.

 SPDX-License-Identifier: EPL-2.0

Author: Christian Rechner, AUDI AG

## Introduction
This application allows accessing the content of an MDF3/MDF4 file via an ASAM ODS Session, backed by an ATFX-File, and the export of this file.
The file contents including Meta-Data are transformed to the ASAM ODS standard and can be accessed using ASAMs OO-API.


## Java code example for opening a ASAM ODS session on an ATFX file

    import java.nio.file.Path;
    import java.nio.file.Paths;
    
    import org.asam.ods.AoSession;
    import org.omg.CORBA.ORB;
    
    import de.rechner.openatfx_mdf.ConvertException;
    import de.rechner.openatfx_mdf.MDFConverter;
    
    public class Example{
        public static void main(String[] args) throws ConvertException{
            ORB orb = ORB.init(new String[0], System.getProperties());
            Path path = Paths.get("C:\\myExample.mf4");
            MDFConverter reader = new MDFConverter();
            AoSession aoSession = reader.getAoSessionForMDF(orb, path);
        }
    }

## Configuration parameters:
### MDF3

### MDF4
* replace_square_brackets (boolean, default=false)<br>
If true, all square brackets ([]) in channel names are replaced by curly brackets ({}).
* use_file_name_as_result_name (boolean, default=true)<br>
If true, the file name is used as result name, otherwise the value of below parameter 'result_name' is used.
* result_name (string, no default)<br>
If parameter 'use_file_name_as_result_name' is true, the value of this parameter is used to name the measurement.
* result_suffix (string, no default)<br>
If provided, this value will be suffixed to the result name (before the .mf4 extension): test.mf4 will become test_123.mf4 with suffix 123
* result_mimetype (string, default="application/x-asam.aomeasurement.timeseries")<br>
The mimetype to set to the created measurement instance.
* attach_source_files (boolean, default=false)<br>
If true, the source mdf file will be attached as reference to the created measurement.
* read_only_header (boolean, default=false)<br>
If true, any event data or mass data that is transformed to AoSubMatrix instances and their children will be skipped.
* skip_scale_conversion_channels (boolean, default=false)<br>
If true, any scaling channel, so a CCBLOCK referenced by another CCBLOCK, is skipped.
* skip_uint64_data_channels (boolean, default=false)<br>
If true, any channel with datatype uint64 is skipped, otherwise an exception is thrown. Such values cannot safely be stored in Java or in ASAM ODS.
* skip_byte_stream_channels (boolean, deafault=false)<br>
If true, any channels with DataType 'byte array' (MDF datatype 10) are skipped.
* write_flags_file (boolean, default=true)<br>
If true, a flags file will be generated, otherwise not.
* skip_empty_channels (boolean, default=false)<br>
If true, all channels without values (cycleCount < 1) are skipped.
* attachments_to_tst (boolean, default=false)<br>
If true, attachment files are connected to the tst instance instead to the mea.
* export_to_subfolder (boolean, default=false)<br>
If true, embedded file streams in AT blocks are transformed to separate files in a subfolder named after the mdf file.
* $${ignore_attachments} (boolean, default=false)<br>
If true, attachments in AT blocks are ignored.
* comment_replacement_strings (string, no default)<br>
A space separated list of key value pairs, separated by colon each. The key is String in the original comment value, the value of the pair is the replacement text. All such key occurrences in any property value of a comment block (TX, MD) will be replaces accordingly. Example: Ã¤:ä Ã„:Ä Ã¼:ü Ãœ:Ü Ã¶:ö Ã–:Ö
* skip_extra_conversion_channels (boolean, default=false)<br>
Some mf4 files may contain additional CGs and channels that are just an explicit representation for a tabular type conversion of some other contained channel. If this parameter is true, these channels will be skipped.
* ignore_unsupported_conversion_types (boolean, default=false)<br>
If true, unsupported conversion types like type 9 or 10 are just skipped, otherwise an exception is thrown.
* skip_previews (boolean, default=true)<br>
If true, SRBLOCKs are transformed to ASAM ODS preview structures, otherwise they are simply ignored.

## Known bugs/missing features:
### BLOCKs
* IDBLOCK
  - id_unfin_flags must be 0
  - id_custom_unfin_flags must be 0
* HDBLOCK
  - hd_ch_first must be 0 (channel hierarchy not yet supported)
  - hd_flags must be 0 [bits 00] (start angle value below is invalid, start distance value below is invalid)
* CHBLOCK (MDF3): not yet supported, will be ignored with warning
* ATBLOCK: not yet supported, will be ignored with warning
* EVBLOCK:
  - ev_ev_parent will be ignored with warning
  - ev_ev_range will be ignored with warning
  - formula tag in ev_md_comment not entirely supported
* DGBLOCK
  - may contain only ONE channel group (sorted MDF), otherwise an exception is thrown
  - Records must not be zipped or split up in multiple datablocks if parts of one record are in multiple blocks.
* CGBLOCK
  - cg_flags must be null (VLSD and bus events not supported), otherwise an exception is thrown
  - cg_inval_bytes must be null (invalidation bits not supported), otherwise an exception is thrown
* CNBLOCK
  - cn_composition only array composition supported, otherwise an exception is thrown
  - cn_at_reference: not yet supported, will be ignored with warning
  - cn_limit_min: not yet supported, will be ignored with warning
  - cn_limit_max: not yet supported, will be ignored with warning
  - cn_limit_ext_min: not yet supported, will be ignored with warning
  - cn_limit_ext_max: not yet supported, will be ignored with warning
  - VLSD only supports String datatypes.


### XML content
* mdf_base.xsd
  - type 'common_properties'
   - 'tree' will be ignored with warning
   - 'list' will be ignored with warning
   - 'elist' will be ignored with warning
* hd_comment.xsd
  - 'UNIT-SPEC' will be ignored with warning